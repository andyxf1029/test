package com.ebaocloud.javatemplate.core;

import com.ebaocloud.javatemplate.obj.APIRequest;
import com.ebaocloud.javatemplate.obj.APIResponse;

/**
 * Created by xuhaiping on 2019-10-14.
 */
public class UserService {
    public APIResponse doGet(String param){

        return APIResponse.ok("Get Method:" + param);
    }

    public APIResponse doPost(APIRequest request){

        return APIResponse.ok("Post Method:" + request.getParam());
    }
}
