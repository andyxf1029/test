package com.ebaocloud.javatemplate;

import com.ebaocloud.javatemplate.core.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by xuhaiping on 17/9/25.
 */
@Configuration
public class ApplicationAutoConfigurer {

    @Bean
    public UserService userService(){
        return new UserService();
    }
}
