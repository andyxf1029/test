package com.ebaocloud.javatemplate.obj;

/**
 * Created by xuhaiping on 2019-10-14.
 */
public enum EnumAPIResponseStatus {
    Ok(0),
    Error(1);

    private int code;

    public int getCode() {
        return code;
    }

    EnumAPIResponseStatus(int code){
        this.code = code;
    }

    public static EnumAPIResponseStatus index(int code)
    {
        EnumAPIResponseStatus[] enums = values();

        for (EnumAPIResponseStatus currentEnum : enums)
        {
            if (currentEnum.code == code)
            {
                return currentEnum;
            }
        }
        return null;
    }
}
