package com.ebaocloud.javatemplate.obj;

/**
 * Created by xuhaiping on 2019-10-14.
 */
public class APIRequest {
    private String param;

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
