package com.ebaocloud.javatemplate.obj;

/**
 * Created by xuhaiping on 2019-10-14.
 */
public class APIResponse {
    private int status;
    private String message;
    private Object body;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public static APIResponse ok(){
        APIResponse response = new APIResponse();
        response.setStatus(EnumAPIResponseStatus.Ok.getCode());
        return response;
    }

    public static APIResponse ok(Object body){
        APIResponse response = new APIResponse();
        response.setStatus(EnumAPIResponseStatus.Ok.getCode());
        response.setBody(body);
        return response;
    }

    public static APIResponse error(String message){
        APIResponse response = new APIResponse();
        response.setStatus(EnumAPIResponseStatus.Error.getCode());
        response.setMessage(message);
        return response;
    }
}
