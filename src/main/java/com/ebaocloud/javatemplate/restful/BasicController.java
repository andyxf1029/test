package com.ebaocloud.javatemplate.restful;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by xuhaiping on 17/9/25.
 */
@RestController
@RequestMapping()
public class BasicController {

    @RequestMapping(method={RequestMethod.GET}, value="swagger.json")
    public ModelAndView swagger(HttpServletRequest request){
        return new ModelAndView("/v2/api-docs");
    }

    @RequestMapping(method={RequestMethod.GET}, value="health")
    public String health(HttpServletRequest request){
        return "OK";
    }
}
