package com.ebaocloud.javatemplate.restful;

import com.ebaocloud.javatemplate.core.UserService;
import com.ebaocloud.javatemplate.obj.APIRequest;
import com.ebaocloud.javatemplate.obj.APIResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by xuhaiping on 17/9/25.
 */
@Api(value = "UserController ")
@RestController
@RequestMapping()
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * demo method
     * @param
     * @return
     */
    @RequestMapping(method={RequestMethod.GET}, value="/api/run")
    @ApiOperation(value="java template get method")
    public APIResponse doGet(String param){
        return userService.doGet(param);
    }

    /**
     * demo method
     * @param request
     * @return
     */
    @RequestMapping(method={RequestMethod.POST}, value="/api/run")
    @ApiOperation(value="java template post method")
    public APIResponse doPost(@RequestBody APIRequest request){
        return userService.doPost(request);
    }
}
